﻿using Front.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Cryptography.Xml;



namespace Front.Services
{
    public class InscriptionService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public InscriptionService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<UserDTO> SignUpUserAsync(string username, string password, string email)
        {
            UserCreateModel model = new UserCreateModel { Name = username, Password = password, Email = email };
            using (var client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:5000/");
                HttpResponseMessage response = await client.PostAsJsonAsync("api/User/register", model);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // You can deserialize the response content here if needed
                    var result = await response.Content.ReadFromJsonAsync<UserDTO>();
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
