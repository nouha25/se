﻿using Front.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Cryptography.Xml;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;

namespace Front.Services
{
    public class LoginService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private ProtectedLocalStorage _sessionStorage;

        public LoginService(IHttpClientFactory httpClientFactory, ProtectedLocalStorage sessionStorage)
        {
            _httpClientFactory = httpClientFactory;
            _sessionStorage = sessionStorage;
        }

        public async Task<UserDTO> AuthenticateUserAsync(string username, string password)
        {
            UserLogin model = new UserLogin { Name = username, Pass = password };
            using (var client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:5000/");
                HttpResponseMessage response = await client.PostAsJsonAsync("api/User/login", model);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // You can deserialize the response content here if needed
                    var result = await response.Content.ReadFromJsonAsync<JWTAndUser>();
                    await _sessionStorage.SetAsync("jwt", result.Token);
                    return result.JWTUser;
                }
                else
                {
                    return null;
                }
            }
        }


        private static UserDTO UserToDTO(User user)
        {
            return new UserDTO
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
            };
        }
        public class JWTAndUser
        {
            public required string Token { get; set; }
            public required UserDTO JWTUser { get; set; }
        }
    }
}
