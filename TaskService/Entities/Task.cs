namespace TaskService.Entities
{
    public class Task
    {
        
        public int Id { get; set; }

        public required string Text { get; set; }

        public bool IsDone { get; set; }
        public int UserId {get; set; }

    }
    public class TaskCreate
    {
        public required string Text { get; set; }
        public bool IsDone { get; set; }
        public int UserId {get; set; }
    }

    public class TaskUpdate
    {
        public required string Text { get; set; }
        public bool IsDone { get; set; }
    }
}
