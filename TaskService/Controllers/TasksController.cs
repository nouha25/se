﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskService.Entities;
using TaskService.Data;
using Microsoft.CodeAnalysis.Elfie.Model.Structures;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaskService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskServiceContext _tasks;
        private int _taskIndex = 0;

        public TasksController(TaskServiceContext tasks)
        {
            _tasks = tasks;
        }

        // GET: api/Tasks
        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<Entities.Task>>> Get(int userId)
        {
            
             return await _tasks.Task
                        .Where(t => t.UserId == userId)
                        .ToListAsync();
        }

        // GET api/Tasks/5
        //[HttpGet("{id}", Name = "Get")]
        /*
        public async Task<ActionResult<Entities.Task>>  Get(int id)
        {
            var task = await _tasks.Task.FindAsync(id);

            if (task == null)
            {
                return NotFound();
            }

            return TaskToDTO(task);
        }
        */
        // POST api/Tasks
        [HttpPost("{userId}")]
        public async Task<ActionResult<Entities.Task>> CreateTask(TaskCreate task, int userId)
        {
            var newtask = new Entities.Task
            {
                Id = _taskIndex++,
                IsDone = task.IsDone,
                Text = task.Text,
                UserId = userId
            };
            _tasks.Task.Add(newtask);
            await _tasks.SaveChangesAsync();
            return Ok(newtask);

        }

        // PUT api/Tasks/5
        [HttpPut("{userId}/{id}")]
        public async Task<IActionResult> Put(int userId , int id, Entities.TaskUpdate taskUpdate)
        {
           
            var task = await _tasks.Task.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }
            task.Text = taskUpdate.Text;
            task.IsDone = taskUpdate.IsDone;
            task.UserId = userId;

            try
            {
                await _tasks.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(task);
        }

        // DELETE api/Tasks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var task = await _tasks.Task.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }
            _tasks.Task.Remove(task);
            await _tasks.SaveChangesAsync();
            return Ok(true);
        }
        private bool TaskExists(int id)
        {
            return _tasks.Task.Any(e => e.Id == id);
        }
        private static Entities.Task TaskToDTO(Entities.Task task)
        {
            return new Entities.Task
            {
                Id = task.Id,
                Text = task.Text,
                IsDone = task.IsDone

            };
        }
    }
}



  