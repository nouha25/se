﻿namespace TaskService.Entities
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public required bool IsDone { get; set; }
        public required string Text { get; set; }
    }

    public class Task
        {
            private Task task;
            public int Id { get; set; }

            public required string Text { get; set; }

            public bool IsDone { get; set; }
            public int UserId { get; set; }

    }
        public class TaskCreate
        {
            public required string Text { get; set; }
            public bool IsDone { get; set; }
        
        }
  }


